# Jenkins Seldon Core

## Environment
- Docker for macos
- minikube

## Usage
- 1
```bash
docker run -d -u root -p 8080:8080 \
	-v /path/to/jenkins/folder:/var/jenkins_home \
	-v /var/run/docker.sock:/var/run/docker.sock \
	-v /path/to/home/.minikube:/root/.minikube \
	-e JAVA_OPTS="-Xms256m -Xmx512m" jenkinsci/blueocean:1.23.0
```
- 2
```
Install Jenkins plugin --> Parameterized Trigger
```
- 3
```bash
git clone git@gitlab.com:johnny-butter/jenkins-seldon-core.git
```
- 4
```bash
cp -R jenkins-seldon-core/. /path/to/jenkins/folder/jobs/
```
- 5
```
Restart Jenkins
```
- 6
```
a. Set target docker hub in CD_Image_Build
b. Set credential for jenkins for pushing commit to gitlab (CD_Update_Deployment)
```
